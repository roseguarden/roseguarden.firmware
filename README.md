# Setup, Build and Flash ESP32 wit IDF on Linux with Eclipse IDE

## Setup the toolchain, the idf and eclipse

* Install the esp-idf and the toolchain [Espressif documentation setup](https://docs.espressif.com/projects/esp-idf/en/latest/get-started-cmake/index.html#installation-step-by-step)
* The source of the next steps is the official [Espressif documentation eclipse](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/eclipse-setup.html)

## Update the toolchain and idf


* go to your esp idf 
  * checkout the master: `git checkout master`
  * pull the new version: `git pull`
  * update the submodules: `git submodule update --init --recursive`
  * update the python requirements with pip (using anaconda3): `/home/roseguarden/anaconda3/bin/python -m pip install -r /home/roseguarden/esp32/esp-idf/requirements.txt`
* go to your xtensa toolchain path and replace the folder with the new version [Xtensa-Toolchain](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/linux-setup.html?highlight=xtensa)


## Installing Eclipse IDE
* When running the Eclipse Installer, choose `Eclipse for C/C++ Development` (in other places you’ll see this referred to as CDT.)

## Setting up Eclipse
Once your new Eclipse installation launches, follow these steps:

### Import New Project
* Eclipse makes use of the Makefile support in ESP-IDF. This means you need to start by creating an ESP-IDF project. You can use the idf-template project from github, or open one of the examples in the esp-idf examples subdirectory.
* Once Eclipse is running, choose `File` -> `Import…`
* In the dialog that pops up, choose `C/C++` -> `Existing Code as Makefile Project` and click `Next`.
* On the next page, enter `Existing Code Location` to be the directory of your IDF project. Don’t specify the path to the ESP-IDF directory itself (that comes later). The directory you specify should contain a file named “Makefile” (the project Makefile).
* On the same page, under `Toolchain for Indexer Settings` choose `Cross GCC`. Then click `Finish`.

### Project Properties
* The new project will appear under Project Explorer. Right-click the project and choose `Properties` from the context menu.
* Click on `C/C++ Build` -> `Environment`. Click `Add…` and enter name `BATCH_BUILD` and value `1`.
* Click `Add…` again, and enter name `IDF_PATH`. The value should be the full path where ESP-IDF is installed. Windows users can copy the IDF_PATH from windows explorer.
    * If you don't remember the `IDF_PATH`, open the terminal and run `printenv IDF_PATH`. The output should be something like `/home/$USER/esp/esp-idf`
* Edit the `PATH` environment variable. Keep the current value, and append the path to the Xtensa toolchain installed as part of IDF setup, if this is not already listed on the PATH. A typical path to the toolchain looks like `/home/user-name/esp/xtensa-esp32-elf/bin`. Note that you need to add a colon : before the appended path. Windows users will need to prepend C:\msys32\mingw32\bin;C:\msys32\opt\xtensa-esp32-elf\bin;C:\msys32\usr\bin to PATH environment variable (If you installed msys32 to a different directory then you’ll need to change these paths to match).
* On macOS, add a `PYTHONPATH` environment variable and set it to `/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages`. This is so that the system Python, which has pyserial installed as part of the setup steps, overrides any built-in Eclipse Python.

Navigate to `C/C++ General` -> `Preprocessor Include Paths` property page:
* Click the `Providers` tab
* In the list of providers, click `CDT Cross GCC Built-in Compiler Settings`. Change `Command to get compiler specs` to `xtensa-esp32-elf-gcc ${FLAGS} -std=c++11 -E -P -v -dD "${INPUTS}"`.
* In the list of providers, click `CDT GCC Build Output Parser` and change the `Compiler command pattern` to `xtensa-esp32-elf-(gcc|g\+\+|c\+\+|cc|cpp|clang)`
* Check all include path to be set for all languages

Navigate to `C/C++ General` -> `Indexer` property page:
* Check `Enable project specific settings` to enable the rest of the settings on this page.
* Uncheck `Allow heuristic resolution of includes`. When this option is enabled Eclipse sometimes fails to find correct header directories.

Navigate to `C/C++ Build` -> `Behavior` property page:
* Check `Enable parallel build` to enable multiple build jobs in parallel.


## Building in Eclipse
Before your project is first built, Eclipse may show a lot of errors and warnings about undefined values. This is because some source files are automatically generated as part of the esp-idf build process. These errors and warnings will go away after you build the project.

* Click OK to close the Properties dialog in Eclipse.
* Outside Eclipse, open a command line prompt. Navigate to your project directory, and run `make menuconfig` to configure your project’s esp-idf settings. Save the esp-idf settings. This step currently has to be run outside Eclipse.

*If you try to build without running a configuration step first, esp-idf will prompt for configuration on the command line - but Eclipse is not able to deal with this, so the build will hang or fail.*

* Back in Eclipse, choose **Project** -> **Build** to build your project.
TIP: If your project had already been built outside Eclipse, you may need to do a **Project** -> **Clean** before choosing **Project** -> **Build**. This is so Eclipse can see the compiler arguments for all source files. It uses these to determine the header include paths.


## Flash from Eclipse

* Add run configurations by activate the right dropdown menu and click on **New Launch Configuration ...**
* **Launch Mode**: `Run`
* **Launch Configuration Type**: `C/C++ Application`
* **Name**: `flash`
* **Project**: Your project name
* **C/C++ Application**: `/usr/bin/gnome-terminal`
	* this line works only, if gnome-terminal is installed
* Go to the page **Arguments**
* **Program arguments**: `-e 'make flash'`
* Rest can be default

## Monitor from Eclipse

* Add run configurations by activate the right dropdown menu and click on **New Launch Configuration ...**
* **Launch Mode**: `Run`
* **Launch Configuration Type**: `C/C++ Application`
* **Name**: `monitor`
* **Project**: Your project name
* **C/C++ Application**: `/usr/bin/gnome-terminal`
	* this line works only, if gnome-terminal is installed
* Disable **Enable auto build**
* Go to the page **Arguments**
* **Program arguments**: `-e 'make monitor'`
* Rest can be default


---


# Steps to Import and  Build a new Project

* have a look at https://docs.espressif.com/projects/esp-idf/en/latest/get-started/eclipse-setup.html

## Set enviroment Variables
BATCH_BUILD = 1 # [enable batch compilation]
IDF_PATH = /home/roseguarden/esp32/esp-idf # [idf path]
PATH  += /home/roseguarden/esp32/xtensa-esp32-elf/bin # [toolchain bin dir]

## Set Provider in “C/C++ General” -> “Preprocessor Include Paths”

* In “CDT Cross GCC Built-in Compiler Settings”. Change “Command to get compiler specs” to  : xtensa-esp32-elf-gcc ${FLAGS} -std=c++11 -E -P -v -dD "${INPUTS}"
- In “CDT GCC Build Output Parser”. Change the “Compiler command pattern” to : xtensa-esp32-elf-(gcc|g\+\+|c\+\+|cc|cpp|clang)

## Change Indexer settings in “C/C++ General” -> “Indexer” 

* Check “Enable project specific settings” to enable the rest of the settings on this page.
* Uncheck “Allow heuristic resolution of includes”. When this option is enabled Eclipse sometimes fails to find correct header directories.

# Add run configuration

* add run configurations for flash, menuconfig and monitor
* Start a Terminal '/usr/bin/gnome-terminal'  with the argument of the command e.g. "-e 'make flash'"
* For menuconfig it is necessary to unset the BATCH_BUILD: -x sh -c 'unset BATCH_BUILD;make menuconfig; exec bash'
* For monitor and menuconfig "disable auto build"


# Run Menuconfig:

* Change the XTAL clock to 'Automatic' detection
* Change the python-interpreter to your specific e.g.:

# Clean and build

use Project -> Clean and than Project -> Build
