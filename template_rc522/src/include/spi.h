/*
 * spi.h
 *
 *  Created on: Jul 12, 2019
 *      Author: M. Drobisch
 */

#ifndef SRC_INCLUDE_SPI_H_
#define SRC_INCLUDE_SPI_H_



#include <stdint.h>
#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "hal/spi_types.h"
#include "esp_err.h"

typedef
class SPI {

private:
	spi_host_device_t _host;
    spi_device_handle_t _device;
	uint64_t _chipselectfield_enable = 0;
	uint64_t _chipselectfield_invert = 0;


public:

	static const spi_host_device_t ESP_SPI = SPI_HOST;
	static const spi_host_device_t ESP_HSPI = HSPI_HOST;
	static const spi_host_device_t ESP_VSPI = VSPI_HOST;

	explicit SPI(spi_host_device_t host);
	~SPI();

	esp_err_t initialize(gpio_num_t mosi_io_num, gpio_num_t miso_io_num, gpio_num_t sclk_io_num, int max_transfer_sz = SPI_MAX_DMA_LEN);
	esp_err_t registerChipSelect(gpio_num_t cs_io_num, bool invert= true);
	esp_err_t unregisterChipSelect(gpio_num_t cs_io_num);
	esp_err_t configure(spi_device_interface_config_t *dev_config);
	esp_err_t configure(uint8_t mode, uint32_t clock_speed_hz, uint32_t flags = 0);
	esp_err_t beginTransaction(gpio_num_t cs_io_num, bool verbose=false);
	esp_err_t endTransaction(gpio_num_t cs_io_num, bool verbose=false);
	esp_err_t transceive(uint8_t *data_tx, uint8_t *data_rx, uint16_t bytes, bool verbose=false);
	esp_err_t transmit(uint8_t *data_tx, uint16_t bytes, bool verbose=false);
	// esp_err_t writeRegister32bit(uint32_t address, uint32_t data_tx);
	// esp_err_t writeRegister16bit(uint16_t address, uint16_t data_tx);
	// esp_err_t writeRegister8bit(uint8_t address, uint8_t data_tx);
	// esp_err_t readRegister32bit(uint32_t address, uint32_t *data_rx);
	// esp_err_t readRegister16bit(uint16_t address, uint16_t *data_rx);
	esp_err_t readRegister8bit(uint8_t address, uint8_t &data_rx);
	esp_err_t writeRegister8bit(uint8_t address, uint8_t data_tx);
	esp_err_t free();


} spi_t;

#endif /* SRC_INCLUDE_SPI_H_ */
