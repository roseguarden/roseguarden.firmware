/*
 * mfrc522.cpp
 *
 *  Created on: Jul 8, 2019
 *      Author: M. Drobisch
 */

#include <mfrc522.h>
#include "esp_log.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

static const char *TAG = "mfrc522";


/**
 * Constructor.
 */
MFRC522::MFRC522(	spi_t *spi,  	// SPI device
					gpio_num_t ss_io_num,	// Chip select pin to be used
					gpio_num_t rst_io_num	// Reset pin to be used
				) {
	_spi = spi;
	_ss_io_num = ss_io_num;
	_rst_io_num = rst_io_num;
}

/*
 * Check the reader connection type and version
 */
mfrc522_status_t MFRC522::checkReader(mfrc522_readerInfo_t &info) {
	uint8_t result;
	readReg(PCD_Register::VersionReg, result);
	info.set(result);
	return STATUS_OK;
}

mfrc522_status_t MFRC522::readReg(PCD_Register reg, uint8_t &result) {

	_spi->beginTransaction(_ss_io_num);
	_spi->readRegister8bit(((reg << 1) & 0x7E) | 0x80, result);
	_spi->endTransaction(_ss_io_num);
	return STATUS_OK;
}


mfrc522_status_t MFRC522::writePCDCommand(PCD_Command command) {
	_spi->beginTransaction(_ss_io_num);
	_spi->writeRegister8bit((PCD_Register::CommandReg << 1) & 0x7E, command);
	_spi->endTransaction(_ss_io_num);
	return STATUS_OK;
}

mfrc522_status_t MFRC522::writeReg(PCD_Register reg, uint8_t data) {
	_spi->beginTransaction(_ss_io_num);
	_spi->writeRegister8bit((reg << 1) & 0x7E, data);
	_spi->endTransaction(_ss_io_num);
	return STATUS_OK;
}

mfrc522_status_t MFRC522::clearRegBits(PCD_Register reg, uint8_t mask) {
	uint8_t tmp;
	readReg(reg, tmp);
	writeReg(reg, tmp & (~mask));
	return STATUS_OK;
}

mfrc522_status_t MFRC522::setRegBits(PCD_Register reg, uint8_t mask) {
	uint8_t tmp;
	readReg(reg, tmp);
	writeReg(reg, tmp | mask);
	return STATUS_OK;
}

mfrc522_status_t MFRC522::initialize(bool boost) {
	_spi->registerChipSelect(_ss_io_num, true);

	/* pad select gpio */
    gpio_pad_select_gpio(_rst_io_num);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(_rst_io_num, GPIO_MODE_OUTPUT);
    /* reset have to be high*/
   	gpio_set_level(_rst_io_num, 1);
    vTaskDelay(10 / portTICK_PERIOD_MS);
    reset();

    writeReg(PCD_Register::TModeReg, 0x8D);
    writeReg(PCD_Register::TPrescalerReg, 0x3E);
    writeReg(PCD_Register::TReloadRegL, 30);
    writeReg(PCD_Register::TReloadRegH, 0);
    writeReg(PCD_Register::TxASKReg, 0x40);
    writeReg(PCD_Register::ModeReg, 0x3D);

    if (boost == true) {
        writeReg(PCD_Register::CWGsPReg, 0x30);
        writeReg(PCD_Register::ModGsPReg, 0x30);
        writeReg(PCD_Register::GsNReg, 0xCC);
        writeReg(PCD_Register::RFCfgReg, 0x68);
        writeReg(PCD_Register::RxThresholdReg, 0x43);
    }


    vTaskDelay(10 / portTICK_PERIOD_MS);
    enableAntenna();
	return STATUS_OK;
}

mfrc522_status_t MFRC522::readPiccUid(mfrc522_picc_t &tag_info) {
	// zero the length
	tag_info.uid_length = 0;

	writeReg(PCD_Register::BitFramingReg, 0x00);
	uint8_t data_tx[2] = {
			PICC_Command::PICC_CMD_SEL_CL1, // Select cascade level 1 (SEL) for 4 byte uid
			0x20							// NVB (Number of valid bits), 2 bytes (SEL+NVB) + 0 additional bits
	};
	uint8_t data_tx_length=2;
	uint8_t data_rx[16];
	uint8_t data_rx_length=0;
	uint8_t i = 0, check = 0;
	mfrc522_status_t sta = transferCommand(PCD_Command::PCD_Transceive, data_tx, data_tx_length, data_rx, data_rx_length);
	if ( sta == STATUS_OK ) {
		if(data_rx_length == 5) {
			for(i = 0; i < 4;i++) {
				check = check ^ data_rx[i];
				tag_info.uid[i] = data_rx[i];
			}
			if (check != data_rx[4]) {
				ESP_LOGE(TAG, "Invalid checksum of %d, expected %d", check, data_rx[4]);
				return STATUS_ERROR;
			} else {
				tag_info.uid_length = 4;
				return STATUS_OK;
			}
		} else {
			ESP_LOGE(TAG, "Invalid size of uid data %d, expected 5", data_rx_length);
			return STATUS_ERROR;
		}
	} else {
		return sta;
	}
}

mfrc522_status_t MFRC522::requestPicc() {
	// set last bit framing to 7
	writeReg(PCD_Register::BitFramingReg, 0x07);
	uint8_t data_tx[1] = { PICC_Command::PICC_CMD_REQA };
	uint8_t data_tx_length=1;
	uint8_t data_rx[16];
	uint8_t data_rx_length=0;
	return transferCommand(PCD_Command::PCD_Transceive, data_tx, data_tx_length, data_rx, data_rx_length);
}

mfrc522_status_t MFRC522::scan(mfrc522_picc_t &tag_info) {

	mfrc522_status_t sta = requestPicc();
	if ( sta == STATUS_OK) {
		return readPiccUid(tag_info);
	} else {
		if (sta != STATUS_NOTAG) {
			ESP_LOGE(TAG, "Requesting PICC failed with an error");
		}
		return sta;
	}
}

mfrc522_status_t MFRC522::transferCommand(PCD_Command command, uint8_t *data_tx, uint8_t &data_tx_length, uint8_t *data_rx, uint8_t &data_rx_length, uint8_t data_rx_length_max) {

	uint8_t irqEn=0, irqReg=0, waitIRQ=0, tmp=0, err=0, lastBits=0, n=0;
	uint16_t i=0;


	switch (command) {
		case PCD_Command::PCD_Transceive:
			irqEn = 0x77;
			waitIRQ = 0x30;
			break;
		case PCD_Command::PCD_MFAuthent:
			irqEn = 0x12;
			waitIRQ = 0x10;
			break;
		default:
			irqEn = 0x00;
			waitIRQ = 0x00;
			break;

	}

	writeReg(PCD_Register::ComIEnReg, irqEn|0x80);
	clearRegBits(PCD_Register::ComIrqReg, 0x80);
	setRegBits(PCD_Register::FIFOLevelReg, 0x80);

	writePCDCommand(PCD_Command::PCD_Idle);

	for(i = 0;i < data_tx_length;i++) {
		writeReg(PCD_Register::FIFODataReg, data_tx[i]);
	}

	writePCDCommand(command);

	if (command == PCD_Command::PCD_Transceive) {
		setRegBits(PCD_Register::BitFramingReg, 0x80);
	}

	// wait for irq to happen and store
	for (i = 2000; i > 0; i--) {
		readReg(PCD_Register::ComIrqReg, irqReg);
		if ((irqReg & 0x01) != 0 || (irqReg & waitIRQ) != 0)
			break;
	}

	#ifdef MFRC522_VERBOSE_LOG
	ESP_LOGW(TAG, "Getting IrqReg 0x%02X IrqEn 0x%02X", irqReg, irqEn);
	#endif

	clearRegBits(PCD_Register::BitFramingReg, 0x80);

	if ( i == 0) {
		ESP_LOGE(TAG, "Getting IRQ timed out");
		return STATUS_TIMEOUT;
	}

	readReg(PCD_Register::ErrorReg, err);
	if ((err & 0x1B) == 0x00) {

		// if ((irqReg & irqEn & 0x01) != 0) {
		if ((irqReg & irqEn & 0x01) != 0) {
			return STATUS_NOTAG;
		} else {
			#ifdef MFRC522_VERBOSE_LOG
			ESP_LOGW(TAG, "Tag detected");
			#endif
		}

		// for transceive get the data
		if (command == PCD_Command::PCD_Transceive) {
			#ifdef MFRC522_VERBOSE_LOG
			ESP_LOGW(TAG, "Read FIFOLevelReg");
			#endif

			readReg(PCD_Register::FIFOLevelReg, n);
			readReg(PCD_Register::ControlReg, lastBits);
			lastBits = lastBits & 0x07;

			if (lastBits != 0) {
				data_rx_length = (n)*8 + lastBits;
			} else {
				data_rx_length = n*8;
			}

			if(n == 0) {
				n = 1;
			}
			if(n > data_rx_length_max) {
				n = data_rx_length_max;
			}

			#ifdef MFRC522_VERBOSE_LOG
			ESP_LOGW(TAG, "Read FIFODataReg for %d bytes", n);
			#endif

			for(i = 0; i < n;i++) {
				readReg(PCD_Register::FIFODataReg, tmp);
				data_rx[i] = tmp;
			}
			data_rx_length = n;
			return STATUS_OK;
		}
		#ifdef MFRC522_VERBOSE_LOG
		ESP_LOGI(TAG, "Transfer command %02X successful", command);
		#endif
		return STATUS_OK;

	} else {
		ESP_LOGE(TAG, "Error detected ErrorReg=%02X", tmp);
		return STATUS_ERROR;
	}
}

mfrc522_status_t MFRC522::enableAntenna() {
	uint8_t tmp=0;
	readReg(PCD_Register::TxControlReg, tmp);
	if((tmp & 0x03) == 0) {
		setRegBits(PCD_Register::TxControlReg,0x03);
	}
	return STATUS_OK;
}
mfrc522_status_t MFRC522::disableAntenna() {
	clearRegBits(PCD_Register::TxControlReg, 0x03);
	return STATUS_OK;
}


mfrc522_status_t MFRC522::reset() {
	writePCDCommand(PCD_Command::PCD_SoftReset);
	return STATUS_OK;
}

