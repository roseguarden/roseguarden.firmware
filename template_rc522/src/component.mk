#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS := . ./spi ./mfrc522
COMPONENT_ADD_INCLUDEDIRS := . ./include