/*
 * MFRC522 Template
 * Created on: Jul 11, 2019
 * Author: M. Drobisch
 *
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "sdkconfig.h"

#include "spi.h"
#include "mfrc522.h"


#define SPI_MODE  3

#define CS_PIN    32
#define SCLK_PIN  16
#define MOSI_PIN  15
#define MISO_PIN  14


#define NRST_PIN  0

#define SPI_CLOCK 100000  // 100 kHz

static const char *TAG = "main";

extern "C" void app_main() {

	ESP_LOGI(TAG, "MFRC522 template");

    spi_t spi(SPI::ESP_VSPI);  // vspi and hspi are the default objects
    vTaskDelay(200 / portTICK_PERIOD_MS);

    ESP_LOGI(TAG, "Initialize spi");
	ESP_ERROR_CHECK( spi.initialize((gpio_num_t)MOSI_PIN, (gpio_num_t)MISO_PIN, (gpio_num_t)SCLK_PIN));
    vTaskDelay(200 / portTICK_PERIOD_MS);

	ESP_LOGI(TAG, "Configure spi");
    ESP_ERROR_CHECK( spi.configure(SPI_MODE, SPI_CLOCK));
    vTaskDelay(200 / portTICK_PERIOD_MS);

	ESP_LOGI(TAG, "Create mfrc522");
    mfrc522_t mfrc522(&spi, (gpio_num_t)CS_PIN, (gpio_num_t)NRST_PIN);
    vTaskDelay(200 / portTICK_PERIOD_MS);
    mfrc522.initialize(true);

    while (1) {
    	mfrc522_readerInfo_t info;
    	mfrc522.checkReader(info);
    	ESP_LOGI(TAG, "ReaderType %d ReaderVersion %d", info.type, info.version);

    	mfrc522_picc_t picc_tag;
    	if (mfrc522.scan(picc_tag) == STATUS_OK) {

    		char print_buf [64];
    		int par = 0;
    		for(int i = 0; i < picc_tag.uid_length;i++) {
    			// log tx buffer to string
    			par += snprintf ( &(print_buf[par]), 128-par, "0x%02X ", picc_tag.uid[i]);
    			if (par <= 0 || par > 128) {
    				break;
    			}
    		}
    		ESP_LOGI(TAG, "Detected Tag with UID: %s", print_buf);
    	}

    	ESP_LOGI(TAG, "Retry");
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }

    spi.free();

    vTaskDelay(portMAX_DELAY);
}
