/*
 * spi.cpp
 *
 *  Created on: Jul 13, 2019
 *      Author: M. Drobisch
 */


#include "spi.h"

#include <stdint.h>
#include "esp_log.h"
#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include "hal/spi_types.h"

#include "esp_err.h"

// Set SPI_DATA_LOG to true for verbose SPI logging informations
#define SPI_DATA_LOG false

static const char *TAG = "spi";

SPI::SPI(spi_host_device_t host) : _host(host) {
}

SPI::~SPI() {
    free();
}

esp_err_t SPI::initialize(gpio_num_t mosi_io_num, gpio_num_t miso_io_num, gpio_num_t sclk_io_num, int max_transfer_sz) {
    spi_bus_config_t config;
    config.mosi_io_num = mosi_io_num;
    config.miso_io_num = miso_io_num;
    config.sclk_io_num = sclk_io_num;
    config.quadwp_io_num = -1;  // -1 not used
    config.quadhd_io_num = -1;  // -1 not used
    config.max_transfer_sz = max_transfer_sz;
    return spi_bus_initialize(_host, &config, 0);
}

esp_err_t SPI::configure(spi_device_interface_config_t *dev_config) {

	return spi_bus_add_device(_host, dev_config, &_device);
}


esp_err_t SPI::configure(uint8_t mode, uint32_t clock_speed_hz, uint32_t flags) {

    spi_device_interface_config_t dev_config;
    dev_config.command_bits = 0;
    dev_config.address_bits = 0;
    dev_config.dummy_bits = 0;
    dev_config.mode = mode;
    dev_config.duty_cycle_pos = 100;  // 50% duty cycle
    dev_config.cs_ena_pretrans = 1;  // cs_ena_pretrans not used
    dev_config.cs_ena_posttrans = 1;  // cs_ena_posttrans not used
    dev_config.clock_speed_hz = clock_speed_hz;
    dev_config.spics_io_num = -1; // not used here, see registerChipSelect()
    dev_config.flags = flags;
    dev_config.queue_size = 1;
    dev_config.pre_cb = NULL;
    dev_config.post_cb = NULL;
    return configure(&dev_config);
}

esp_err_t SPI::registerChipSelect(gpio_num_t cs_io_num, bool invert) {

	_chipselectfield_enable |= 0x01 << ((int)cs_io_num);
	/* pad select gpio */
    gpio_pad_select_gpio(cs_io_num);
    /* Set the GPIO as a push/pull output */
    gpio_set_direction(cs_io_num, GPIO_MODE_OUTPUT);

    if(invert == false) {
		_chipselectfield_invert &= ~(0x01 << ((int)cs_io_num));
    	gpio_set_level(cs_io_num, 0);
	} else {
		_chipselectfield_invert |= 0x01 << ((int)cs_io_num);
    	gpio_set_level(cs_io_num, 1);
	}

	return ESP_OK;
}

esp_err_t SPI::unregisterChipSelect(gpio_num_t cs_io_num) {
	_chipselectfield_enable &= ~(0x01 << ((int)cs_io_num));
    gpio_set_direction(cs_io_num, GPIO_MODE_INPUT);
	return ESP_OK;
}

esp_err_t SPI::readRegister8bit(uint8_t address, uint8_t &data_rx) {
	uint8_t tx_buffer[2] = { address, 0x00};
	uint8_t rx_buffer[2];
    transceive(tx_buffer,rx_buffer, 2);
    data_rx = rx_buffer[1];
	return ESP_OK;
}

esp_err_t SPI::writeRegister8bit(uint8_t address, uint8_t data_tx) {
	uint8_t tx_buffer[2] = { address, data_tx };
	transmit(tx_buffer, 2);
	return ESP_OK;
}



esp_err_t SPI::beginTransaction(gpio_num_t cs_io_num, bool verbose) {
	if ((_chipselectfield_enable & (0x01 << ((int)cs_io_num))) != 0) {
		if ((_chipselectfield_invert & (0x01 << ((int)cs_io_num))) == 0  ) {
			if (verbose == true) {
				ESP_LOGI(TAG, "Begin with CS %d", cs_io_num);
			}
	    	gpio_set_level(cs_io_num, 1);
		} else {
			if (verbose == true) {
				ESP_LOGI(TAG, "Begin with CS %d (inv.)", cs_io_num);
			}
	    	gpio_set_level(cs_io_num, 0);
		}
		return ESP_OK;
	} else {
		ESP_LOGE(TAG, "CS %d not registered", cs_io_num);
		return ESP_FAIL;
	}
}

esp_err_t SPI::endTransaction(gpio_num_t cs_io_num, bool verbose) {
	if ((_chipselectfield_enable & (0x01 << ((int)cs_io_num))) != 0) {
		if ((_chipselectfield_invert & (0x01 << ((int)cs_io_num))) == 0  ) {
			if (verbose == true) {
				ESP_LOGI(TAG, "End with CS %d", cs_io_num);
			}
	    	gpio_set_level(cs_io_num, 0);
		} else {
			if (verbose == true) {
				ESP_LOGI(TAG, "End with CS %d (inv.)", cs_io_num);
			}
			gpio_set_level(cs_io_num, 1);
		}
		return ESP_OK;
	} else {
		ESP_LOGE(TAG, "ChipSelect %d not registered", cs_io_num);
		return ESP_FAIL;
	}
}

esp_err_t SPI::transceive(uint8_t *data_tx, uint8_t *data_rx, uint16_t bytes, bool verbose) {
    spi_transaction_t transaction;
    transaction.flags = 0;
    transaction.cmd = 0;
    transaction.addr = 0;
    transaction.length = bytes*8;
    transaction.rxlength = 0;
    transaction.user = NULL;
    transaction.tx_buffer = data_tx;
    transaction.rx_buffer = data_rx;
    esp_err_t err = spi_device_transmit(_device, &transaction);

    if (err != ESP_OK) {
		ESP_LOGE(TAG, "Error while transmit data");
    	return err;
    }

	if (verbose == true) {
		char tx_buffer[128], rx_buffer[128];
		int tx_par = 0, rx_par = 0;
		for(int i = 0; i < bytes;i++) {
			// log tx data to string
			tx_par += snprintf ( &(tx_buffer[tx_par]), 128-tx_par, "0x%02X ", data_tx[i]);
			if (tx_par <= 0 || tx_par > 128) {
				break;
			}
			// log rx data to string
			rx_par += snprintf ( &(rx_buffer[rx_par]), 128-rx_par, "0x%02X ", data_rx[i]);
			if (rx_par <= 0 || rx_par > 128) {
				break;
			}
		}

		if (rx_par <= 0 || rx_par > 128 || tx_par <= 0 || tx_par > 128) {
			ESP_LOGE(TAG, "error parse transmit data");
			return ESP_FAIL;
		}
		else {
			ESP_LOGI(TAG, "transmit %d bits : %s", bytes, tx_buffer);
			ESP_LOGI(TAG, "received %d bits : %s", bytes, rx_buffer);
		}
	}

	return ESP_OK;
}

esp_err_t SPI::transmit(uint8_t *data_tx, uint16_t bytes, bool verbose) {
    spi_transaction_t transaction;
    transaction.flags = 0;
    transaction.cmd = 0;
    transaction.addr = 0;
    transaction.length = bytes*8;
    transaction.rxlength = 0;
    transaction.user = NULL;
    transaction.tx_buffer = data_tx;
    transaction.rx_buffer = NULL;
    esp_err_t err = spi_device_transmit(_device, &transaction);

    if (err != ESP_OK) {
		ESP_LOGE(TAG, "error while transmit data");
    	return err;
    }

	if (verbose == true) {
		char tx_buffer [128];
		int par = 0;
		for(int i = 0; i < bytes;i++) {
			// log tx buffer to string
			par += snprintf ( &(tx_buffer[par]), 128-par, "0x%02X ", data_tx[i]);
			if (par <= 0 || par > 128) {
				break;
			}
		}

		if (par <= 0 || par > 128) {
			ESP_LOGE(TAG, "error parse transmit data");
			return ESP_FAIL;
		}
		else {
			ESP_LOGI(TAG, "transmit %d bits : %s", bytes, tx_buffer);
		}
	}


	return ESP_OK;
}


esp_err_t SPI::free() {
    return spi_bus_free(_host);
}

