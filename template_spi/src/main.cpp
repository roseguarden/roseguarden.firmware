/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "sdkconfig.h"

#include "spi.h"


#define SPI_MODE  0
#define MISO_PIN  17
#define MOSI_PIN  5
#define SCLK_PIN  23
#define CS_PIN    16
#define SPI_CLOCK 1000000  // 1 MHz

static const char *TAG = "main";

extern "C" void app_main() {
    printf("SPIbus Example \n");
    fflush(stdout);


    spi_t spi(SPI::ESP_VSPI);  // vspi and hspi are the default objects
	ESP_LOGI(TAG, "Initialize spi");
    ESP_ERROR_CHECK( spi.initialize((gpio_num_t)MOSI_PIN, (gpio_num_t)MISO_PIN, (gpio_num_t)SCLK_PIN));
    vTaskDelay(200 / portTICK_PERIOD_MS);

	ESP_LOGI(TAG, "Configure spi");
    ESP_ERROR_CHECK( spi.configure(SPI_MODE, SPI_CLOCK));


	ESP_LOGI(TAG, "Configure CS");
	spi.registerChipSelect((gpio_num_t)CS_PIN, true);

    while (1) {

    	// test register write
    	ESP_LOGI(TAG, "Write register addr=0xAA val=0xBB");
    	spi.beginTransaction((gpio_num_t)CS_PIN);
    	spi.writeRegister8bit(0xAA, 0xBB);
    	spi.endTransaction((gpio_num_t)CS_PIN);

    	ESP_LOGI(TAG, "Tranceive buffer");
        uint8_t rx_buffer[4], tx_buffer[4] = { 0x01, 0x02, 0x03, 0x04};
    	spi.beginTransaction((gpio_num_t)CS_PIN);
    	spi.transceive(tx_buffer, rx_buffer, 4, true);
    	spi.endTransaction((gpio_num_t)CS_PIN);

    	vTaskDelay(1000 / portTICK_PERIOD_MS);
    	ESP_LOGI(TAG, "retry");

    }

    spi.unregisterChipSelect((gpio_num_t)CS_PIN);
    spi.free();

    vTaskDelay(portMAX_DELAY);
}
