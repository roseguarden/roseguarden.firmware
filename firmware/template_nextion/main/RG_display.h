#ifndef RG_DISPLAY_H
#define RG_DISPLAY_H

#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"

#include "HardwareSerialWrapper.h"
#include "../ITEADLIB_Arduino_Nextion/Nextion.h"
#include <string.h>

#include "esp_log.h"


void numpadButton(char symbol);

void b1PopCallback(void *ptr);

void b2PopCallback(void *ptr);

void b3PopCallback(void *ptr);

void b4PopCallback(void *ptr);

void b5PopCallback(void *ptr);

void b6PopCallback(void *ptr);

void b7PopCallback(void *ptr);

void b8PopCallback(void *ptr);

void b9PopCallback(void *ptr);

void b0PopCallback(void *ptr);

void bAsteriskPopCallback(void *ptr);

void bNumberSignPopCallback(void *ptr);

void bDeletePopCallback(void *ptr);

void resetPasswordInput();

void display_setup(void);



// extern "C" {
//     void app_main();
// // }


// void app_main() {
//   // Das muss in die Main vor der statemachine  setup();
//     while (1) {
//       nexLoop(nex_listen_list);
#endif