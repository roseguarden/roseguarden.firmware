/*
 * HardwareSerialWrapper.h
 *
 *  Created on: Jul 12, 2019
 *      Author: M. Drobisch
 */

#ifndef SRC_INCLUDE_SPI_H_
#define SRC_INCLUDE_SPI_H_



#include <stdint.h>
#include "driver/spi_common.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "hal/spi_types.h"
#include "esp_err.h"

#define TXD_PIN (GPIO_NUM_32)
#define RXD_PIN (GPIO_NUM_33)

void delay(long time);

typedef
class HardwareSerialWrapper {

private:
	char* rxBuffer;
	unsigned int timeout = 5;
	static const int rxBufferSize = 512;

public:

	explicit HardwareSerialWrapper();
	~HardwareSerialWrapper();

	esp_err_t initialize(gpio_num_t gpio_tx, gpio_num_t gpio_rx, int baud);

	size_t write(char c);

	size_t write(const char* buffer, size_t size);

	size_t write(const char* buffer);

	int read(void);
	int available(void);
	//int print()
	void setTimeout(uint32_t timeout);
	size_t readBytes(char *buffer, size_t size);
	void begin(unsigned long baud, uint32_t config, int8_t rxPin, int8_t txPin, bool invert, unsigned long timeout_ms);
	void begin(unsigned long baud);
	esp_err_t free();


} Serial_t;

extern HardwareSerialWrapper Serial2;
#endif /* SRC_INCLUDE_SPI_H_ */
