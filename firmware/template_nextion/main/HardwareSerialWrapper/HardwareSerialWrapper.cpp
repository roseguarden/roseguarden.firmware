/*
 * HardwareSerialWrapper.cpp
 *
 *  Created on: Jul 13, 2019
 *      Author: M. Drobisch
 */


#include "HardwareSerialWrapper.h"
#include "esp_log.h"
#include "driver/uart.h"
#include <string.h>

static const int RX_BUF_SIZE = 1024;

void delay(long time) {

}



	HardwareSerialWrapper::HardwareSerialWrapper()
    {
        rxBuffer = (char*) malloc(rxBufferSize);


        // void init() {
            const uart_config_t uart_config = {
                .baud_rate = 115200,
                .data_bits = UART_DATA_8_BITS,
                .parity = UART_PARITY_DISABLE,
                .stop_bits = UART_STOP_BITS_1,
                .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
            };
            uart_param_config(UART_NUM_1, &uart_config);
            uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
            // We won't use a buffer for sending data.
            uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
        // }
    }

	HardwareSerialWrapper::~HardwareSerialWrapper()
    {

    }

	esp_err_t HardwareSerialWrapper::initialize(gpio_num_t gpio_tx, gpio_num_t gpio_rx, int baud)
    {
        return 0;
    }

	size_t HardwareSerialWrapper::write(char c)
    {
        const int txByte = uart_write_bytes(UART_NUM_1, &c, 1);
        ESP_LOGI("SEND", "Wrote %d byte", txByte);
        return txByte;
        // return 0;
    }

	size_t HardwareSerialWrapper::write(const char* buffer)
    {
        const int len = strlen(buffer);
        const int txBytes = uart_write_bytes(UART_NUM_1, buffer, len);
        ESP_LOGI("SEND", "Wrote %d bytes", txBytes);
        return txBytes;
        // return 0;
    }

    int HardwareSerialWrapper::read(void)
    {
        int rxByte=0;
        ESP_LOGI("READ", "read");
        size_t bytes = uart_read_bytes(UART_NUM_1, (uint8_t*) &rxByte, 1, timeout / portTICK_RATE_MS);
        ESP_LOGI("READ", "read one byte: %02X", rxByte);
        return rxByte;
    }
	
    int HardwareSerialWrapper::available(void)
    {
        size_t size = 0;
        uart_get_buffered_data_len(UART_NUM_1, &size);

        return (int)size;
    }
	
    // not necessary because write functions does more or less the same
    //int print()
	

    void HardwareSerialWrapper::setTimeout(uint32_t timeout)
    {
        timeout = timeout;
    }
	
    size_t HardwareSerialWrapper::readBytes(char *buffer, size_t size)
    {
        ESP_LOGI("READ", "readBytes");
        size_t read = uart_read_bytes(UART_NUM_1, (uint8_t*)rxBuffer, rxBufferSize, timeout / portTICK_RATE_MS);  
        ESP_LOGI("READ", "Read %d bytes", read);
        return read;
    }

	void HardwareSerialWrapper::begin(unsigned long baud)
    {
        uart_set_baudrate(UART_NUM_1, (uint32_t) baud);
    }

	// void HardwareSerialWrapper::begin(unsigned long baud, uint32_t config, int8_t rxPin, int8_t txPin, bool invert, unsigned long timeout_ms)
    // {

    // }

	// esp_err_t HardwareSerialWrapper::free()
    // {
    //     uart_isr_free(UART_NUM_1);
    //     return 0;
    // }