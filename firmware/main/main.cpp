#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include "RG_network.h"
#include "RG_rfid.h"
#include "RG_Server.h"
#include "cJSON.h"

// Zeug von Kevin
#include "RG_display.h"

#define NODE_NAME "Door1"


static const char *TAG = "wifi softAP";
static const char *STATE = "STATE";

static const int RX_BUF_SIZE2 = 1024;
static const int ST_IDLE = 1;
static const int ST_BREAK = 2;
static const int ST_HEART_BEAT = 3;
static const int ST_HTTPS_REQ = 4;
// State Machine
uint8_t* data = 0;

int ACTUAL_STATE = ST_IDLE;
int NEXT_STATE = ST_IDLE;

//Idle State counter
int64_t starttime;
int64_t currenttime;
int64_t idle_time_in_seconds;

//cJSON
cJSON *JSON_OBJ;
char httpBuffer[1024];
char REQUEST_STRING[1024];
char *REQUEST_BODY = NULL;



// extern "C" {


// void RG_generate_heart_beat(cJSON *JSON_OBJ);

// void RG_send_json(char *string);

// void RG_send_request(char *REQUEST);

// void RG_generate_request(char *REQUEST, char *BODY);


// };

// void *pvParameters;
/* Constants that aren't configurable in menuconfig */
#define WEB_SERVER "www.cloud.fabba.space"
#define WEB_PORT "443"
#define WEB_URL "https://roseguarden.fabba.space/api/v1/nodes"

static const char *HTTPS_TAG = "HTTPS";


static char *REQUEST = "POST " WEB_URL " HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n";

/* Root cert for howsmyssl.com, taken from server_root_cert.pem

   The PEM file was extracted from the output of this command:
   openssl s_client -showcerts -connect www.howsmyssl.com:443 </dev/null

   The CA root cert is the last cert given in the chain of certs.

   To embed it in the app binary, the PEM file is named
   in the component.mk COMPONENT_EMBED_TXTFILES variable.
*/
extern const uint8_t server_root_cert_pem_start[] asm("_binary_server_root_cert_pem_start");
extern const uint8_t server_root_cert_pem_end[]   asm("_binary_server_root_cert_pem_end");

extern "C" {
  void app_main();
}

void app_main(void)
{
    RG_rfid_init();
    data = (uint8_t*) malloc(RX_BUF_SIZE2+1);
    // xTaskCreate(RG_rfid_rx_task, "uart_rx_task", 1024*2, NULL, configMAX_PRIORITIES, NULL);
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "ESP_WIFI_MODE_AP");
    wifi_init_sta();
    char *card1 = "-AAAD401C";
    char *card2 = "-AA22182C";
    char *card3 = "-AA22182C";
    while (1)
    {
      switch (ACTUAL_STATE)
      {
      case ST_IDLE:
        starttime = esp_timer_get_time(); //uptime in us
        ESP_LOGI(STATE, "State: ST_IDLE ");
        while (1)
        {         
            if(RG_read_card(data) == 0)
            {          
              if(strncmp(card2, (const char *)&data[2], 6) == 0){
              NEXT_STATE = ST_HEART_BEAT;
              }
              else
              {
              NEXT_STATE = ST_HTTPS_REQ;
              }
              break;
            }
            else
            {
              currenttime = esp_timer_get_time();
              idle_time_in_seconds = (currenttime - starttime)/1000000;
              if (idle_time_in_seconds > 120)
              {
                NEXT_STATE = ST_HEART_BEAT;
                break;
              }
                           
            }
            
        };
        break;
      
      case ST_BREAK:
        ESP_LOGI(STATE, "State: ST_BREAK");
        vTaskDelay(1000);
        NEXT_STATE=ST_HEART_BEAT;
        break;

      case ST_HEART_BEAT:
        ESP_LOGI(STATE, "State: ST_HEART_BEAT");
        // Generate JSON with heartbeat content
        cJSON_Delete(JSON_OBJ);
        JSON_OBJ = cJSON_CreateObject();
        RG_generate_heart_beat(JSON_OBJ);
        // Generate String to send as POST request
        REQUEST_BODY = cJSON_PrintUnformatted(JSON_OBJ);
        RG_generate_request(REQUEST_STRING, REQUEST_BODY);
        
        // snprintf(httpBuffer, 1023,
        // "POST %s HTTP/1.0\r\n"
        // "Host: %s \r\n"
        // "User-Agent: esp-idf/1.0 esp32\r\n"
        // "Content-Type: application/json;charset=utf-8\r\n"
        // "Content-Length: %d\r\n"
        // "Accept: /\r\n"
        // "\r\n%s \r\n",
        // WEB_URL, WEB_SERVER, strlen(cJSON_PrintUnformatted(JSON_OBJ)), cJSON_PrintUnformatted(JSON_OBJ));
        // printf(httpBuffer);

        RG_send_request(REQUEST_STRING);
        NEXT_STATE=ST_IDLE;
        break;

      case ST_HTTPS_REQ:
        ESP_LOGI(STATE, "State: ST_HTTPS_REQ");
        RG_send_request(REQUEST);
        NEXT_STATE = ST_IDLE;
        break;



      default:
        break;
      }
      ACTUAL_STATE = NEXT_STATE;
    }
}
