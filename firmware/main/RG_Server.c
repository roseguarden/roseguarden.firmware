/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/* HTTPS GET Example using plain mbedTLS sockets
 *
 * Contacts the howsmyssl.com API via TLS v1.2 and reads a JSON
 * response.
 *
 * Adapted from the ssl_client1 example in mbedtls.
 *
 * Original Copyright (C) 2006-2016, ARM Limited, All Rights Reserved, Apache 2.0 License.
 * Additions Copyright (C) Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD, Apache 2.0 License.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include "cJSON.h"
#include "esp_timer.h"

#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
//#include "protocol_examples_common.h"
#include "esp_netif.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "esp_tls.h"

#include "RG_Server.h"

char https_buffer[1024];

void RG_generate_heart_beat(cJSON *JSON_OBJ)
{
    int64_t uptime = esp_timer_get_time(); //uptime in us

    cJSON *head, *actions, *heart_beat;
    head = cJSON_CreateObject();
    actions = cJSON_CreateArray();
    cJSON_AddItemToObject(head, "version", cJSON_CreateString("1.0"));
    cJSON_AddItemToObject(head, "targettype", cJSON_CreateString("server"));
    cJSON_AddItemToObject(head, "source", cJSON_CreateString("NODE_NAME"));
    cJSON_AddItemToObject(head, "sourcetype", cJSON_CreateString("node"));
    cJSON_AddItemToObject(head, "nodetype", cJSON_CreateString("door"));
    cJSON_AddItemToObject(head, "nodeauth", cJSON_CreateString("thisisateststring"));
    cJSON_AddItemToObject(head, "fingerprint", cJSON_CreateString(NODE_FINGERPRINT));
    cJSON_AddItemToObject(head, "uptime", cJSON_CreateNumber(uptime));
    cJSON_AddItemToObject(JSON_OBJ, "head", head);
    
    cJSON_AddItemToArray(actions, heart_beat = cJSON_CreateObject());
    cJSON_AddItemToObject(heart_beat, "version", cJSON_CreateString("1.0"));
    cJSON_AddItemToObject(heart_beat, "action", cJSON_CreateString("checkHeartbeat"));
    cJSON_AddItemToObject(JSON_OBJ, "actions", actions);

        char* out = cJSON_Print(JSON_OBJ);
        printf("%s\n", out);
}

void RG_send_json(char* string){

}

static const char *HTTPS_TAG = "HTTPS";
static const char *REQUEST = "POST" WEB_URL " HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n"
    "\r\n";

/* Root cert for howsmyssl.com, taken from server_root_cert.pem

   The PEM file was extracted from the output of this command:
   openssl s_client -showcerts -connect www.howsmyssl.com:443 </dev/null

   The CA root cert is the last cert given in the chain of certs.

   To embed it in the app binary, the PEM file is named
   in the component.mk COMPONENT_EMBED_TXTFILES variable.
*/
extern const uint8_t server_root_cert_pem_start[] asm("_binary_server_root_cert_pem_start");
extern const uint8_t server_root_cert_pem_end[]   asm("_binary_server_root_cert_pem_end");


void RG_send_request(char *REQUEST)
{
    char buf[1024];
    int ret, len;
    while(1) {
        esp_tls_cfg_t cfg = {
            .cacert_buf  = server_root_cert_pem_start,
            .cacert_bytes = server_root_cert_pem_end - server_root_cert_pem_start,
        };
        
        struct esp_tls *tls = esp_tls_conn_http_new(WEB_URL, &cfg);
        
        if(tls != NULL) {
            ESP_LOGI(HTTPS_TAG, "Connection established...");
        } else {
            ESP_LOGE(HTTPS_TAG, "Connection failed...");
            goto exit;
        }
        
        size_t written_bytes = 0;
        do {
            ret = esp_tls_conn_write(tls, 
                                     REQUEST + written_bytes, 
                                     strlen(REQUEST) - written_bytes);
            
            if (ret >= 0) {
                ESP_LOGI(HTTPS_TAG, "%d bytes written", ret);
                written_bytes += ret;
            } else if (ret != ESP_TLS_ERR_SSL_WANT_READ  && ret != ESP_TLS_ERR_SSL_WANT_WRITE) {
                ESP_LOGE(HTTPS_TAG, "esp_tls_conn_write  returned 0x%x", ret);
                goto exit;
            }
        } while(written_bytes < strlen(REQUEST));

        ESP_LOGI(HTTPS_TAG, "Reading HTTP response...");

        do
        {
            len = sizeof(buf) - 1;
            bzero(buf, sizeof(buf));
            ret = esp_tls_conn_read(tls, (char *)buf, len);
            
            if(ret == ESP_TLS_ERR_SSL_WANT_WRITE  || ret == ESP_TLS_ERR_SSL_WANT_READ)
                continue;
            
            if(ret < 0)
           {
                ESP_LOGE(HTTPS_TAG, "esp_tls_conn_read  returned -0x%x", -ret);
                break;
            }

            if(ret == 0)
            {
                ESP_LOGI(HTTPS_TAG, "connection closed");
                break;
            }

            len = ret;
            ESP_LOGD(HTTPS_TAG, "%d bytes read", len);
            /* Print response directly to stdout as it is read */
            for(int i = 0; i < len; i++) {
                putchar(buf[i]);
            }
            putchar('\n');
        } while(1);

    exit:
        esp_tls_conn_delete(tls);    
        putchar('\n'); // JSON output doesn't have a newline at end

        static int request_count;
        ESP_LOGI(HTTPS_TAG, "Completed %d requests", ++request_count);

        break;
    }
}
void RG_generate_request(char* REQUEST,char* BODY){
    snprintf(REQUEST, 1024,
        "POST %s HTTP/1.0\r\n"
        "Host: %s \r\n"
        "User-Agent: esp-idf/1.0 esp32\r\n"
        "Content-Type: application/json;charset=utf-8\r\n"
        "Content-Length: %d\r\n"
        "Accept: /\r\n"
        "\r\n"
        "%s \r\n",
        WEB_URL, WEB_SERVER, strlen(BODY), BODY);
}