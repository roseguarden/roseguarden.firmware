#ifdef __cplusplus
#define EXTERN_C extern "C" {
#define EXTERN_C_END }
#else
#define EXTERN_C
#define EXTERN_C_END
#endif
#ifndef RG_SERVER_H
#define RG_SERVER_H

#define NODE_FINGERPRINT "TestFingerprint"
#define WEB_SERVER "www.cloud.fabba.space"
#define WEB_PORT "443"
#define WEB_URL "https://roseguarden.fabba.space/api/v1/nodes"


#include "cJSON.h"


//TLS
#include <string.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_system.h"
#include "nvs_flash.h"
//#include "protocol_examples_common.h"
#include "esp_netif.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "esp_tls.h"

EXTERN_C


void RG_generate_heart_beat(cJSON *JSON_OBJ);

void RG_send_json(char *string);

void RG_send_request(char *REQUEST);

void RG_generate_request(char *REQUEST, char *BODY);

EXTERN_C_END
#endif