#ifdef __cplusplus
#define EXTERN_C extern "C" {
#define EXTERN_C_END }
#else
#define EXTERN_C
#define EXTERN_C_END
#endif

#ifndef RG_RFID_READER_H
#define RG_RFID_READER_H

// static const int RX_BUF_SIZE = 1024;

// #define TXD_PIN (GPIO_NUM_4) //Pins fuer BreakoutBoad von Joern
// #define RXD_PIN (GPIO_NUM_5)

EXTERN_C

void RG_rfid_init();

int RG_rfid_sendData(const char* logName, const char* data);

static void RG_rfid_tx_task(void);

void RG_rfid_rx_task(void);

int RG_read_card(uint8_t* data);

EXTERN_C_END
#endif
