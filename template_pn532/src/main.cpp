/* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "sdkconfig.h"

#include "pn532.h"


static const char *TAG = "main";

extern "C" void app_main() {
    printf("PN532 Example \n");
    fflush(stdout);


    while (1) {

    	/*
    	 * Not finished yet
    	 * the pn532 lib is located in pn532.cpp and need
    	 * to be used in this file here
		 */

    	// init_PN532_I2C();

    	vTaskDelay(1000 / portTICK_PERIOD_MS);
    	ESP_LOGI(TAG, "retry");

    }

    vTaskDelay(portMAX_DELAY);
}
