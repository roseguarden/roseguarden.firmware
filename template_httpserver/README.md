# HTTP Server template

## Prepare the spiffs file table

To pack a folder into 1 Megabyte image:

`mkspiffs -c [src_folder] -b 4096 -p 256 -s 0x100000 spiffs.bin`

To flash the image to ESP32 at offset 0x110000:

`python2 esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 115200 write_flash -z 0x110000 spiffs.bin`


