#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS := . ./server 
COMPONENT_ADD_INCLUDEDIRS := . ./include
COMPONENT_EMBED_FILES := ../web/favicon.ico
COMPONENT_EMBED_FILES += ../web/index.html
COMPONENT_EMBED_FILES += ../web/embedded/upload.html
