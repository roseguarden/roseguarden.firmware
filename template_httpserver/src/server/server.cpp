/*
 * server.cpp
 *
 *  Created on: Dec 27, 2019
 *      Author: roseguarden
 */

#include "server.h"

httpd_uri_t ctrl = {
    .uri       = "/ctrl",
    .method    = HTTP_PUT,
    .handler   = ctrl_put_handler,
    .user_ctx  = NULL
};

httpd_uri_t hello = {
    .uri       = "/hello",
    .method    = HTTP_GET,
    .handler   = hello_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = (void*)"Hello World!"
};

httpd_uri_t echo = {
    .uri       = "/echo",
    .method    = HTTP_POST,
    .handler   = echo_post_handler,
    .user_ctx  = NULL
};


/* URI handler for uploading files to server */
httpd_uri_t file_upload_post = {
	.uri       = "/upload/*",   // Match all URIs of type /upload/path/to/file
	.method    = HTTP_POST,
	.handler   = upload_post_handler,
	.user_ctx  = server_data    // Pass server data as context
};

/* URI handler for getting the uploading-site of the files to server */
httpd_uri_t file_upload_get = {
	.uri       = "/upload",   // Match all URIs of type /upload/path/to/file
	.method    = HTTP_GET,
	.handler   = upload_get_handler,
	.user_ctx  = server_data    // Pass server data as context
};

/* URI handler for getting uploaded files */
httpd_uri_t file_download = {
    .uri       = "/*",  // Match all URIs of type /path/to/file
    .method    = HTTP_GET,
    .handler   = download_get_handler,
    .user_ctx  = server_data    // Pass server data as context
};

/* URI handler for deleting files from server */
httpd_uri_t file_delete = {
    .uri       = "/delete/*",   // Match all URIs of type /delete/path/to/file
    .method    = HTTP_POST,
    .handler   = delete_post_handler,
    .user_ctx  = server_data    // Pass server data as context
};
