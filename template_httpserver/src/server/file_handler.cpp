/*
 * file_handler.cpp
 *
 *  Created on: Dec 27, 2019
 *      Author: roseguarden
 */

#include <stdio.h>
#include <string.h>
#include <sys/param.h>
#include <sys/unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include "esp_vfs.h"

#include <esp_wifi.h>
#include <esp_event_loop.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
#include <sys/param.h>
#include <esp_http_server.h>
#include "sdkconfig.h"

#include "server.h"

struct file_server_data *server_data = NULL;

static const char *TAG = "server";

/* Copies the full path into destination buffer and returns
 * pointer to path (skipping the preceding base path) */
const char* get_path_from_uri(char *dest, const char *base_path, const char *uri, size_t destsize)
{
    const size_t base_pathlen = strlen(base_path);
    size_t pathlen = strlen(uri);

    const char *quest = strchr(uri, '?');
    if (quest) {
        pathlen = MIN(pathlen, quest - uri);
    }
    const char *hash = strchr(uri, '#');
    if (hash) {
        pathlen = MIN(pathlen, hash - uri);
    }

    if (base_pathlen + pathlen + 1 > destsize) {
        /* Full path string won't fit into destination buffer */
        return NULL;
    }

    /* Construct full path (base + path) */
    strcpy(dest, base_path);
    strlcpy(dest + base_pathlen, uri, pathlen + 1);

    /* Return pointer to path, skipping the base */
    return dest + base_pathlen;
}

/* Send HTTP response with a run-time generated html consisting of
 * a list of all files and folders under the requested path.
 * In case of SPIFFS this returns empty list when path is any
 * string other than '/', since SPIFFS doesn't support directories */
esp_err_t http_resp_upload_page(httpd_req_t *req)
{
	const char *dirpath = "/spiffs/";

    char entrypath[FILE_PATH_MAX];
    char entrysize[16];
    const char *entrytype;

    struct dirent *entry;
    struct stat entry_stat;

    DIR *dir = opendir(dirpath);
    const size_t dirpath_len = strlen(dirpath);

    /* Retrieve the base path of file storage to construct the full path */
    strlcpy(entrypath, dirpath, sizeof(entrypath));

    if (!dir) {
        ESP_LOGE(TAG, "Failed to stat dir : %s", dirpath);
        /* Respond with 404 Not Found */
        httpd_resp_send_404(req);
        return ESP_FAIL;
    }

    /* Send HTML file header */
    httpd_resp_sendstr_chunk(req, "<!DOCTYPE html><html><body>");

    /* Get handle to embedded file upload script */
    extern const unsigned char upload_script_start[] asm("_binary_upload_html_start");
    extern const unsigned char upload_script_end[]   asm("_binary_upload_html_end");
    const size_t upload_script_size = (upload_script_end - upload_script_start);

    /* Add file upload form and script which on execution sends a POST request to /upload */
    httpd_resp_send_chunk(req, (const char *)upload_script_start, upload_script_size);

    /* Send file-list table definition and column labels */
    httpd_resp_sendstr_chunk(req,
        "<table class=\"fixed\" border=\"1\">"
        "<col width=\"800px\" /><col width=\"300px\" /><col width=\"300px\" /><col width=\"100px\" />"
        "<thead><tr><th>Name</th><th>Type</th><th>Size (Bytes)</th><th>Delete</th></tr></thead>"
        "<tbody>");

    /* Iterate over all files / folders and fetch their names and sizes */
    while ((entry = readdir(dir)) != NULL) {
        entrytype = (entry->d_type == DT_DIR ? "directory" : "file");

        ESP_LOGI(TAG, "File found %s uri: %s type=%s", entry->d_name, req->uri, entrytype);

        strlcpy(entrypath + dirpath_len, entry->d_name, sizeof(entrypath) - dirpath_len);
        if (stat(entrypath, &entry_stat) == -1) {
            ESP_LOGE(TAG, "Failed to stat %s : %s err: %d", entrytype, entry->d_name, errno);
            continue;
        }

        sprintf(entrysize, "%ld", entry_stat.st_size);

		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Found %s : %s (%s bytes)", entrytype, entry->d_name, entrysize);
		#endif

        /* Send chunk of HTML file containing table entries with file name and size */
        httpd_resp_sendstr_chunk(req, "<tr><td><a href=\"");
        httpd_resp_sendstr_chunk(req, req->uri);
        httpd_resp_sendstr_chunk(req, entry->d_name);
        if (entry->d_type == DT_DIR) {
            httpd_resp_sendstr_chunk(req, "/");
        }
        httpd_resp_sendstr_chunk(req, "\">");
        httpd_resp_sendstr_chunk(req, entry->d_name);
        httpd_resp_sendstr_chunk(req, "</a></td><td>");
        httpd_resp_sendstr_chunk(req, entrytype);
        httpd_resp_sendstr_chunk(req, "</td><td>");
        httpd_resp_sendstr_chunk(req, entrysize);
        httpd_resp_sendstr_chunk(req, "</td><td>");
        httpd_resp_sendstr_chunk(req, "<form method=\"post\" action=\"/delete/");
        httpd_resp_sendstr_chunk(req, entry->d_name);
        httpd_resp_sendstr_chunk(req, "\"><button type=\"submit\">Delete</button></form>");
        httpd_resp_sendstr_chunk(req, "</td></tr>\n");
    }
    closedir(dir);

    /* Finish the file list table */
    httpd_resp_sendstr_chunk(req, "</tbody></table>");

    /* Send remaining chunk of HTML file to complete it */
    httpd_resp_sendstr_chunk(req, "</body></html>");

    /* Send empty chunk to signal HTTP response completion */
    httpd_resp_sendstr_chunk(req, NULL);
    return ESP_OK;
}

/* Set HTTP response content type according to file extension */
esp_err_t set_content_type_from_file(httpd_req_t *req, const char *filename)
{
    if (IS_FILE_EXT(filename, ".pdf")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: pdf");
		#endif
        return httpd_resp_set_type(req, "application/pdf");
    } else if (IS_FILE_EXT(filename, ".html")) {
		#if VERBOSE_SERVER_LOG
    		ESP_LOGI(TAG, "Set type: html for %s", filename);
		#endif
        return httpd_resp_set_type(req, "text/html");
    } else if (IS_FILE_EXT(filename, ".jpeg")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: jpeg");
		#endif
        return httpd_resp_set_type(req, "image/jpeg");
    } else if (IS_FILE_EXT(filename, ".ico")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: ico");
		#endif
        return httpd_resp_set_type(req, "image/x-icon");
    } else if (IS_FILE_EXT(filename, ".js")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: js for %s", filename);
		#endif
        return httpd_resp_set_type(req, "application/javascript");
    } else if (IS_FILE_EXT(filename, ".js.gz")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: js for %s", filename);
		#endif
        return httpd_resp_set_type(req, "application/javascript");
    } else if (IS_FILE_EXT(filename, ".css")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: css for %s", filename);
		#endif
        return httpd_resp_set_type(req, "text/css");
    } else if (IS_FILE_EXT(filename, ".css.gz")) {
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Set type: css for %s", filename);
		#endif
        return httpd_resp_set_type(req, "text/css");
    }

    /* This is a limited set only */
    /* For any other type always set as plain text */
	#if VERBOSE_SERVER_LOG
    	ESP_LOGI(TAG, "Set type: text/plain for %s", filename);
	#endif

    return httpd_resp_set_type(req, "text/plain");
}

/* Handler to download a file kept on the server */
esp_err_t download_get_handler(httpd_req_t *req)
{
    char filepath[FILE_PATH_MAX];
    char compressed_filepath[FILE_PATH_MAX];
    FILE *fd = NULL;
    struct stat file_stat;

    const char *filename = get_path_from_uri(filepath, "/spiffs",
                                             req->uri, sizeof(filepath));
    if (!filename) {
        ESP_LOGE(TAG, "Filename is too long");
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    /* If name has trailing '/', respond with directory contents */
    if (filename[strlen(filename) - 1] == '/') {
        ESP_LOGE(TAG, "Request on directory not allowed : %s path : %s", filename, filepath);
    	if (strcmp(filename, "/") == 0) {
			#if VERBOSE_SERVER_LOG
            	ESP_LOGI(TAG, "Request index.html : %s path : %s", filename, filepath);
			#endif
            snprintf(filepath, FILE_PATH_MAX, "%s", "/spiffs/index.html");
            filename = &filepath[strlen("/index.html") - 1];
			#if VERBOSE_SERVER_LOG
            	ESP_LOGI(TAG, "Reset filename : %s path : %s", filename, filepath);
			#endif
        } else {
        	httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "No directories available");
        }
    }

    if (stat(filepath, &file_stat) == -1) {
		#if VERBOSE_SERVER_LOG
    		ESP_LOGI(TAG, "Failed to stat file : %s", filepath);
		#endif
        snprintf(compressed_filepath, FILE_PATH_MAX, "%s.gz", filepath);
		#if VERBOSE_SERVER_LOG
        	ESP_LOGI(TAG, "Check compressed file : %s", filepath);
		#endif
        if (stat(compressed_filepath, &file_stat) == -1) {
        	ESP_LOGE(TAG, "Failed to stat file : %s", compressed_filepath);
            httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "File does not exist");
           	return ESP_FAIL;
         } else {
			#if VERBOSE_SERVER_LOG
        	 ESP_LOGI(TAG, "Serve compressed file : %s", compressed_filepath);
			#endif
        	httpd_resp_set_hdr(req, "Content-Encoding", "gzip");
        	strlcpy(filepath, compressed_filepath, strlen(compressed_filepath)+1);
         }
         /* Respond with 404 Not Found */
	}

	fd = fopen(filepath, "r");
	if (!fd) {
	 ESP_LOGE(TAG, "Failed to read existing file : %s", filepath);
	 /* Respond with 500 Internal Server Error */
	 httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to read existing file");
	 return ESP_FAIL;
	}

	#if VERBOSE_SERVER_LOG
		ESP_LOGI(TAG, "Sending file : %s (%ld bytes)...", filename, file_stat.st_size);
	#endif
	set_content_type_from_file(req, filename);

	/* Retrieve the pointer to scratch buffer for temporary storage */
	char *chunk = ((struct file_server_data *)req->user_ctx)->scratch;
	size_t chunksize;
	do {
		/* Read file in chunks into the scratch buffer */
		chunksize = fread(chunk, 1, SCRATCH_BUFSIZE, fd);

		/* Send the buffer contents as HTTP response chunk */
		if (httpd_resp_send_chunk(req, chunk, chunksize) != ESP_OK) {
			fclose(fd);
			ESP_LOGE(TAG, "File sending failed!");
			/* Abort sending file */
			httpd_resp_sendstr_chunk(req, NULL);
			/* Respond with 500 Internal Server Error */
			httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Failed to send file");
			return ESP_FAIL;
		}
	 /* Keep looping till the whole file is sent */
	} while (chunksize != 0);

	/* Close file after sending complete */
	fclose(fd);
	#if VERBOSE_SERVER_LOG
		ESP_LOGI(TAG, "File sending complete");
	#endif
	/* Respond with an empty chunk to signal HTTP response completion */
	httpd_resp_send_chunk(req, NULL, 0);
	return ESP_OK;

}

/* Handler to upload a file onto the server */
esp_err_t upload_get_handler(httpd_req_t *req)
{
    ESP_LOGI(TAG, "GET-Request on upload");

	return http_resp_upload_page(req);
}

/* Handler to upload a file onto the server */
esp_err_t upload_post_handler(httpd_req_t *req)
{
    char filepath[FILE_PATH_MAX];
    FILE *fd = NULL;
    struct stat file_stat;

    ESP_LOGE(TAG, "Upload getting handled");
    ESP_LOGI(TAG, "URI : %s ", req->uri);

    /* Skip leading "/upload" from URI to get filename */
     /* Note sizeof() counts NULL termination hence the -1 */
    const char *filename = get_path_from_uri(filepath, "/spiffs",
                                             req->uri + sizeof("/upload") - 1, sizeof(filepath));

    ESP_LOGI(TAG, "Filepath : %s ", filepath);

    if (!filename) {
        /* Respond with 500 Internal Server Error */
    	httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    /* Filename cannot have a trailing '/' */
    if (filename[strlen(filename) - 1] == '/') {
        ESP_LOGE(TAG, "Invalid filename : %s", filename);
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    if (stat(filepath, &file_stat) == 0) {
        ESP_LOGE(TAG, "File already exists : %s", filepath);
        /* Respond with 400 Bad Request */
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    /* File cannot be larger than a limit */
    if (req->content_len > MAX_FILE_SIZE) {
        ESP_LOGE(TAG, "File too large : %d bytes", req->content_len);
        /* Respond with 400 Bad Request */
        httpd_resp_send_500(req);
        /* Return failure to close underlying connection else the
         * incoming file content will keep the socket busy */
        return ESP_FAIL;
    }

    fd = fopen(filepath, "w");
    if (!fd) {
        ESP_LOGE(TAG, "Failed to create file : %s", filepath);
        /* Respond with 500 Internal Server Error */
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "Receiving file : %s...", filename);

    /* Retrieve the pointer to scratch buffer for temporary storage */
    char *buf = ((struct file_server_data *)req->user_ctx)->scratch;
    int received;

    /* Content length of the request gives
     * the size of the file being uploaded */
    int remaining = req->content_len;

    while (remaining > 0) {

        ESP_LOGI(TAG, "Remaining size : %d", remaining);
        /* Receive the file part by part into a buffer */
        if ((received = httpd_req_recv(req, buf, MIN(remaining, SCRATCH_BUFSIZE))) <= 0) {
            if (received == HTTPD_SOCK_ERR_TIMEOUT) {
                /* Retry if timeout occurred */
                continue;
            }

            /* In case of unrecoverable error,
             * close and delete the unfinished file*/
            fclose(fd);
            unlink(filepath);

            ESP_LOGE(TAG, "File reception failed!");
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_500(req);
            return ESP_FAIL;
        }

        /* Write buffer content to file on storage */
        if (received && (received != fwrite(buf, 1, received, fd))) {
            /* Couldn't write everything to file!
             * Storage may be full? */
            fclose(fd);
            unlink(filepath);

            ESP_LOGE(TAG, "File write failed!");
            /* Respond with 500 Internal Server Error */
            httpd_resp_send_500(req);
            return ESP_FAIL;
        }

        /* Keep track of remaining size of
         * the file left to be uploaded */
        remaining -= received;
    }

    /* Close file upon upload completion */
    fclose(fd);
    ESP_LOGI(TAG, "File reception complete");

    /* Redirect onto root to see the updated file list */
    httpd_resp_set_status(req, "303 See Other");
    httpd_resp_set_hdr(req, "Location", "/upload");
    const char *resp = "File uploaded successfully";
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}

/* Handler to delete a file from the server */
esp_err_t delete_post_handler(httpd_req_t *req)
{
    char filepath[FILE_PATH_MAX];
    struct stat file_stat;

    /* Skip leading "/delete" from URI to get filename */
    /* Note sizeof() counts NULL termination hence the -1 */
    ESP_LOGI(TAG, "DELETE request on : %s", req->uri);
    const char *filename = get_path_from_uri(filepath, "/spiffs",
                                             req->uri  + sizeof("/delete") - 1, sizeof(filepath));
    if (!filename) {
        /* Respond with 500 Internal Server Error */
    	httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    /* Filename cannot have a trailing '/' */
    if (filename[strlen(filename) - 1] == '/') {
        ESP_LOGE(TAG, "Invalid filename : %s", filename);
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    if (stat(filepath, &file_stat) == -1) {
        ESP_LOGE(TAG, "File does not exist : %s", filename);
        /* Respond with 400 Bad Request */
        httpd_resp_send_500(req);
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "Deleting file : %s", filename);
    /* Delete file */
    unlink(filepath);

    /* Redirect onto root to see the updated file list */
    httpd_resp_set_status(req, "303 See Other");
    httpd_resp_set_hdr(req, "Location", "/upload");
    const char *resp = "File deleted successfully";
    httpd_resp_send(req, resp, strlen(resp));
    return ESP_OK;
}


