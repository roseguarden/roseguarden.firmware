/*
 * server.h
 *
 *  Created on: Dec 27, 2019
 *      Author: roseguarden
 */

#ifndef SRC_INCLUDE_SERVER_H_
#define SRC_INCLUDE_SERVER_H_

#include <esp_log.h>
#include <esp_http_server.h>
#include <esp_system.h>
#include "esp_vfs.h"


#define VERBOSE_SERVER_LOG FALSE // FALSE

/* Scratch buffer size */
#define SCRATCH_BUFSIZE  8192
/* Max length a file path can have on storage */
#define FILE_PATH_MAX (ESP_VFS_PATH_MAX + CONFIG_SPIFFS_OBJ_NAME_LEN)

/* Max size of an individual file. Make sure this
 * value is same as that set in upload_script.html */
#define MAX_FILE_SIZE   (200*1024) // 200 KB
#define MAX_FILE_SIZE_STR "200KB"

#define IS_FILE_EXT(filename, ext) \
    (strcasecmp(&filename[strlen(filename) - sizeof(ext) + 1], ext) == 0)


struct file_server_data {
    /* Base path of file storage */
    char base_path[ESP_VFS_PATH_MAX + 1];

    /* Scratch buffer for temporary storage during file transfer */
    char scratch[SCRATCH_BUFSIZE];
};


esp_err_t hello_get_handler(httpd_req_t *req);
esp_err_t echo_post_handler(httpd_req_t *req);
esp_err_t ctrl_put_handler(httpd_req_t *req);
esp_err_t upload_post_handler(httpd_req_t *req);
esp_err_t upload_get_handler(httpd_req_t *req);
esp_err_t download_get_handler(httpd_req_t *req);
esp_err_t delete_post_handler(httpd_req_t *req);
const char* get_path_from_uri(char *dest, const char *base_path, const char *uri, size_t destsize);

extern file_server_data *server_data;
extern httpd_uri_t ctrl;
extern httpd_uri_t echo;
extern httpd_uri_t hello;
extern httpd_uri_t file_upload_get;
extern httpd_uri_t file_upload_post;
extern httpd_uri_t file_download;
extern httpd_uri_t file_delete;



typedef
class ServerHTTP {

public:
	esp_err_t free();

} server_t;

#endif /* SRC_INCLUDE_SERVER_H_ */
